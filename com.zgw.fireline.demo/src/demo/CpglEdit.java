package demo;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.wb.swt.SWTResourceManager;

import com.zgw.fireline.base.controls.DataCombo;
import com.zgw.fireline.base.controls.DataTable;
import com.zgw.fireline.base.controls.DataTableColumn;
import com.zgw.fireline.base.dataset.Dataset;
import com.zgw.fireline.base.frame.IFunction;
import com.zgw.fireline.base.widgets.Batch;
import com.zgw.fireline.base.widgets.BatchItem;

public class CpglEdit implements IFunction {
	public static final int Add = 1;
	public static final int Edit = 2;
	private Text textCpbh;
	private Text textCpmc;
	private Text textTm;
	private Text textGg;
	private Text textJj;
	private Text textSj;
	private Text textBz;
	private int type = 1;
	public int result = 0;

	public CpglEdit(int type, Dataset set) {
		this.type = type;
		if (type == Edit) {
			cpxxSet.populate(set, set.getSelectedIndex());
			cpxxSet.setSelected(0);
		}
	}

	/**
	 * @wbp.nonvisual location=27,362
	 */
	private final Dataset cpxxSet = new Dataset(true, "cpxx",
			SqliteJdbcImpl.INSTANCE);
	/**
	 * @wbp.nonvisual location=87,362
	 */
	private final Dataset cplb = new Dataset("select * from cplb",
			SqliteJdbcImpl.INSTANCE);
	/**
	 * @wbp.nonvisual location=147,362
	 */
	private final Dataset cpdw = new Dataset("select * from dw",
			SqliteJdbcImpl.INSTANCE);
	private DataCombo ComboLb;
	private DataCombo comboDw;
	/**
	 * @wbp.nonvisual location=217,362
	 */
	private final Dataset tzcp = new Dataset(
			"select t1.cpbh,t1.cpmc ,t0.sl, (t0.sl*t1.jj) jj\r\n from tzcp t0\r\nleft join cpxx t1 on t0.cpbh=t1.cpbh\r\n where t0.tzbh=? ",
			SqliteJdbcImpl.INSTANCE);
	/**
	 * @wbp.nonvisual location=23,422
	 */
	private final Batch cpxxEdit = new Batch(SqliteJdbcImpl.INSTANCE);
	/**
	 * @wbp.nonvisual location=103,422
	 */
	private final Batch cpxxAdd = new Batch(SqliteJdbcImpl.INSTANCE);
	private Composite content;
	private DataTable tableTzcp;
	/**
	 * @wbp.nonvisual location=277,362
	 */
	private final Dataset newId = new Dataset(
			"select max(cpbh)+1 newId from cpxx\r\nwhere length(cpbh)=5\r\n",
			SqliteJdbcImpl.INSTANCE);
	private Button btnTtzh;
	private Button btnCheckButton;

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public Control createControl(Composite parent) {
		cpxxSet.setKeyColumns(new String[] {});
		cplb.setKeyColumns(new String[] {});
		cpdw.setKeyColumns(new String[] {});
		tzcp.setKeyColumns(new String[] {});
		{
			BatchItem batchItem = new BatchItem(cpxxEdit);
			batchItem
					.setCommand("UPDATE cpxx\r\nSET cpmc=?, jm=? ,tm=? ,lb=?, dw=?, \r\ngg=?,  jj=?, sj=?, bz=?,tzzh=?\r\nWHERE cpbh=?");
			batchItem.setName("修改产品基本信息");
		}
		{
			BatchItem batchItem = new BatchItem(cpxxEdit);
			batchItem.setCommand("delete from tzcp where tzbh=?");
			batchItem.setName("删除产品对应的套装记录");
		}
		newId.setKeyColumns(new String[] {});
		{
			BatchItem batchItem = new BatchItem(cpxxAdd);
			batchItem
					.setCommand("insert into cpxx\r\n (cpbh,cpmc, jm ,tm ,lb, dw, gg,  jj, sj, bz,tzzh)\r\nvalues(?,?,?,?,?,?,?,?,?,?,?)");
			batchItem.setName("新增产品信息");
		}
		{
			BatchItem batchItem = new BatchItem(cpxxAdd);
			batchItem.setCommand(" \r\n delete from tzcp where tzbh=?");
			batchItem.setName("删除对应的套装产品");
		}
		final Composite content = new Composite(parent, SWT.NONE);
		content.setLayout(new GridLayout(1, false));

		Group composite = new Group(content, SWT.NONE);
		composite.setText("产品基本信息");
		GridLayout gl_composite = new GridLayout(4, false);
		gl_composite.verticalSpacing = 15;
		gl_composite.horizontalSpacing = 0;
		composite.setLayout(gl_composite);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
				1, 1));
		{
			Label lblNewLabel = new Label(composite, SWT.NONE);
			lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
					false, false, 1, 1));
			lblNewLabel.setText("产品编号：");
		}
		{
			textCpbh = new Text(composite, SWT.BORDER);
			textCpbh.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		{
			Label label = new Label(composite, SWT.NONE);
			{
				GridData gd_label = new GridData(SWT.RIGHT, SWT.CENTER, false,
						false, 1, 1);
				gd_label.horizontalIndent = 20;
				label.setLayoutData(gd_label);
			}
			label.setText("产品名称：");
		}
		{
			textCpmc = new Text(composite, SWT.BORDER);
			textCpmc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("产品条码：");
		}
		{
			textTm = new Text(composite, SWT.BORDER);
			textTm.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("产品类别：");
		}
		{
			Composite composite_1 = new Composite(composite, SWT.NONE);
			composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
					false, 1, 1));
			GridLayout gl_composite_1 = new GridLayout(2, false);
			gl_composite_1.horizontalSpacing = 3;
			gl_composite_1.verticalSpacing = 0;
			gl_composite_1.marginWidth = 0;
			gl_composite_1.marginHeight = 0;
			composite_1.setLayout(gl_composite_1);
			{
				ComboLb = new DataCombo(composite_1, SWT.NONE, cplb);
				ComboLb.setShowText("${lbmc}");
				ComboLb.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
						true, 1, 1));
			}
			{
				Button button = new Button(composite_1, SWT.NONE);
				button.setText("+");
			}
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("产品单位：");
		}
		{
			Composite composite_1 = new Composite(composite, SWT.NONE);
			composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
					false, 1, 1));
			GridLayout gl_composite_1 = new GridLayout(2, false);
			gl_composite_1.marginWidth = 0;
			gl_composite_1.verticalSpacing = 0;
			gl_composite_1.marginHeight = 0;
			gl_composite_1.horizontalSpacing = 3;
			composite_1.setLayout(gl_composite_1);
			{
				comboDw = new DataCombo(composite_1, SWT.NONE, cpdw);
				comboDw.setShowText("${dw}");
				comboDw.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
						true, 1, 1));
			}
			{
				Button btnNewButton_2 = new Button(composite_1, SWT.NONE);
				btnNewButton_2.setText("+");
			}
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("产品规格：");
		}
		{
			textGg = new Text(composite, SWT.BORDER);
			textGg.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("进货价格：");
		}
		{
			textJj = new Text(composite, SWT.BORDER);
			textJj.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("销售价格：");
		}
		{
			textSj = new Text(composite, SWT.BORDER);
			textSj.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		{
			Label label = new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			label.setText("备注信息：");
		}
		{
			textBz = new Text(composite, SWT.BORDER);
			textBz.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 3, 1));
		}
		{
			btnTtzh = new Button(composite, SWT.CHECK);
			btnTtzh.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			btnTtzh.setText("套装组合");
		}
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Group group = new Group(content, SWT.NONE);
		group.setLayout(new GridLayout(2, false));
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		group.setText("套装组合列表");
		{
			tableTzcp = new DataTable(group, SWT.BORDER | SWT.FULL_SELECTION,
					tzcp);
			tableTzcp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
					true, 1, 1));
			tableTzcp.setHeaderVisible(true);
			tableTzcp.setLinesVisible(true);
			{
				DataTableColumn dataTableColumn = new DataTableColumn(
						tableTzcp, SWT.NONE, "cpbh");
				dataTableColumn.setText("产品编号");
				dataTableColumn.setWidth(80);
			}
			{
				DataTableColumn dataTableColumn = new DataTableColumn(
						tableTzcp, SWT.NONE, "cpmc");
				dataTableColumn.setText("产品名称");
				dataTableColumn.setWidth(120);
			}
			{
				DataTableColumn dataTableColumn = new DataTableColumn(
						tableTzcp, SWT.NONE, "sl");
				dataTableColumn.setText("数量");
				dataTableColumn.setWidth(80);
			}
			{
				DataTableColumn dataTableColumn = new DataTableColumn(
						tableTzcp, SWT.NONE, "jj");
				dataTableColumn.setText("价值");
				dataTableColumn.setWidth(80);
			}
		}
		{
			ToolBar toolBar = new ToolBar(group, SWT.FLAT | SWT.RIGHT
					| SWT.VERTICAL);
			toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, true,
					1, 1));
			{
				ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
				tltmNewItem.setText("添加");
				tltmNewItem.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						doAddtzcp();
					};
				});
			}
			{
				ToolItem tltmNewItem_3 = new ToolItem(toolBar, SWT.NONE);
				tltmNewItem_3.setText("修改");
			}
			{
				ToolItem tltmNewItem_2 = new ToolItem(toolBar, SWT.NONE);
				tltmNewItem_2.setText("删除");
			}
		}
		{
			Label lblNewLabel_1 = new Label(group, SWT.NONE);
			{
				GridData gd_lblNewLabel_1 = new GridData(SWT.LEFT, SWT.CENTER,
						false, false, 1, 1);
				gd_lblNewLabel_1.horizontalIndent = 5;
				lblNewLabel_1.setLayoutData(gd_lblNewLabel_1);
			}
			lblNewLabel_1.setForeground(SWTResourceManager
					.getColor(SWT.COLOR_BLUE));
			lblNewLabel_1.setText("由 0 件产品组成套装，总价值 0 元");
		}
		new Label(group, SWT.NONE);

		Composite composite_1 = new Composite(content, SWT.NONE);
		composite_1.setLayout(new GridLayout(2, false));
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		btnCheckButton = new Button(composite_1, SWT.CHECK);
		btnCheckButton.setText("保存后继续添加");
		btnCheckButton.setVisible(type == Add);
		Composite composite_2 = new Composite(composite_1, SWT.NONE);
		composite_2.setLayout(new RowLayout(SWT.HORIZONTAL));
		composite_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
				false, 1, 1));

		Button btnNewButton_1 = new Button(composite_2, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSave();
			}
		});
		btnNewButton_1.setLayoutData(new RowData(80, SWT.DEFAULT));
		btnNewButton_1.setText("保存");

		Button btnNewButton = new Button(composite_2, SWT.NONE);
		btnNewButton.setLayoutData(new RowData(80, SWT.DEFAULT));
		btnNewButton.setBounds(0, 0, 72, 22);
		btnNewButton.setText("退出");
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = SWT.CANCEL;
				content.dispose();
			}
		});
		setParams();
		createGUIAfter();
		setValues();
		this.content = content;
		return content;
	}

	// 添加套装产品
	protected void doAddtzcp() {
	
	}

	protected void doSave() {

		if (type == Edit) {
			setParams();
			// 生成套装产品的添加处理项
			for (TableItem item : tableTzcp.getItems()) {
				BatchItem batc = new BatchItem(cpxxEdit);
				batc.setCommand("insert into tzcp(tzbh,cpbh,sl) values(?,?,?)");
				batc.setName("添加产品套装信息");
				batc.setParam(0, textCpbh.getText()); // 套装编号
				batc.setParam(1, item.getText(0)); // 产品编号
				batc.setParam(2, item.getText(2)); // 数量
			}
			cpxxEdit.execute();
			result = SWT.OK;
			content.dispose();
		} else if (type == Add) {
			setParams();
			// 生成套装产品的添加处理项
			for (TableItem item : tableTzcp.getItems()) {
				BatchItem addTzcpItem = new BatchItem(cpxxAdd);
				addTzcpItem
						.setCommand("insert into tzcp(tzbh,cpbh,sl) values(?,?,?)");
				addTzcpItem.setName("添加产品套装信息");
				addTzcpItem.setParam(0, textCpbh.getText()); // 套装编号
				addTzcpItem.setParam(1, item.getText(0)); // 产品编号
				addTzcpItem.setParam(2, item.getText(2)); // 数量
			}
			cpxxAdd.execute();
			result = SWT.OK;
			content.dispose();
		}
	}

	protected void setParams() {
		tzcp.setParam(0, cpxxSet.getValue("cpbh"));
		cpxxEdit.setParam(0, 0, textCpmc.getText());
		cpxxEdit.setParam(0, 1, textCpmc.getText());
		cpxxEdit.setParam(0, 2, textTm.getText());
		cpxxEdit.setParam(0, 3, ComboLb.getText());
		cpxxEdit.setParam(0, 4, comboDw.getText());
		cpxxEdit.setParam(0, 5, textGg.getText());
		cpxxEdit.setParam(0, 6, textJj.getText());
		cpxxEdit.setParam(0, 7, textSj.getText());
		cpxxEdit.setParam(0, 8, textBz.getText());
		cpxxEdit.setParam(0, 9, btnTtzh.getSelection());
		cpxxEdit.setParam(0, 10, textCpbh.getText());
		cpxxEdit.setParam(1, 0, textCpbh.getText());
		cpxxAdd.setParam(0, 0, textCpbh.getText());
		cpxxAdd.setParam(0, 1, textCpmc.getText());
		cpxxAdd.setParam(0, 2, textCpmc.getText());
		cpxxAdd.setParam(0, 3, textTm.getText());
		cpxxAdd.setParam(0, 4, ComboLb.getText());
		cpxxAdd.setParam(0, 5, comboDw.getText());
		cpxxAdd.setParam(0, 6, textGg.getText());
		cpxxAdd.setParam(0, 7, textJj.getText());
		cpxxAdd.setParam(0, 8, textSj.getText());
		cpxxAdd.setParam(0, 9, textBz.getText());
		cpxxAdd.setParam(0, 10, btnTtzh.getSelection());
	}

	protected void createGUIAfter() {
		cplb.update();
		cpdw.update();
		setParams();
		tzcp.update(); // 套装产品
	}

	protected void setValues() {
		textCpbh.setText(cpxxSet.getValue("cpbh", String.class, ""));
		textCpmc.setText(cpxxSet.getValue("cpmc", String.class, ""));
		textTm.setText(cpxxSet.getValue("tm", String.class, ""));
		ComboLb.setText(cpxxSet.getValue("lb", String.class, ""));
		comboDw.setText(cpxxSet.getValue("dw", String.class, ""));
		textGg.setText(cpxxSet.getValue("gg", String.class, ""));
		textJj.setText(cpxxSet.getValue("jj", String.class, ""));
		textSj.setText(cpxxSet.getValue("sj", String.class, ""));
		textBz.setText(cpxxSet.getValue("bz", String.class, ""));
		if (type == Add) { // 设置产品编号
			newId.update();
			if (newId.getCount() > 0) {
				Integer id = newId.getValueForIndex(0, "newId", Integer.class);
				DecimalFormat df = new DecimalFormat("00000");
				textCpbh.setText(df.format(id));
				textCpbh.selectAll();
			}
		}
		btnTtzh.setSelection(cpxxSet.getValue("tzzh", Boolean.class, false));
	}
}
