package com.zgw.fireline.base.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.zgw.fireline.base.IDesignOnLine;

/**
 * 程序部件信息窗口
 * */
public class WidgetInfoDialog extends Dialog {

	protected Object result;
	protected Shell shell;
	protected IDesignOnLine widget;
	private Tree tree;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public WidgetInfoDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM);
		shell.setSize(218, 403);
		shell.setText("属性窗口");
		shell.setLayout(new GridLayout(1, false));

//		tree = new Tree(shell, SWT.BORDER);
//		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
//		tree.setHeaderVisible(true);
//		tree.setLinesVisible(true);
//
//		TreeColumn trclmnNewColumn = new TreeColumn(tree, SWT.NONE);
//		trclmnNewColumn.setText("名称");
//		trclmnNewColumn.setWidth(100);
//
//		TreeColumn trclmnNewColumn_1 = new TreeColumn(tree, SWT.NONE);
//		trclmnNewColumn_1.setText("值");
//		trclmnNewColumn_1.setWidth(100);
//
//		Button btnNewButton = new Button(shell, SWT.NONE);
//		btnNewButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
//				false, 1, 1));
//		btnNewButton.setText("编辑");
//		btnNewButton.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				doEdit();
//			}
//		});
//		Button btnNewButton_1 = new Button(shell, SWT.NONE);
//		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				doSubmit();
//
//			}
//		});
//		btnNewButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
//				false, 1, 1));
//		btnNewButton_1.setText("提交修改");
//
//		Button btnNewButton_2 = new Button(shell, SWT.NONE);
//		btnNewButton_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
//				false, 1, 1));
//		btnNewButton_2.setText("清除环境");
//		btnNewButton_2.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				doClear();
//			}
//		});
//		Button button = new Button(shell, SWT.NONE);
//		button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
//				1, 1));
//		button.setText("刷新环境");
//		button.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				doRefresh();
//			}
//		});

	}

//	/**
//	 * 重新布署资源至运行环境
//	 * <ul>
//	 * <li>清理本地临时目录
//	 * <li>重新构建资源，并同步至本地临时目录
//	 * </ul>
//	 * */
//	protected void doRefresh() {
//		try {
//			IDesignCommonService service = RmiBeanFactory
//					.getBean(IDesignCommonService.class);
//			File tempBin = new File(Bootstrap.getNativeTemp(), "bin");
//			FileUtil.clearChildren(tempBin);
//			// 重新构建资源
//			service.cleanBuild(Bootstrap.getProperty(Bootstrap.ID),
//					Bootstrap.getNativeTemp());
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 清理开发环境和运行环境
//	 * <ul>
//	 * <li>清理本地临时目录
//	 * <li>清理开发环境src下的资源
//	 * </ul>
//	 * */
//	protected void doClear() {
//		try {
//			IDesignCommonService service = RmiBeanFactory
//					.getBean(IDesignCommonService.class);
//			File tempBin = new File(Bootstrap.getNativeTemp(), "bin");
//			FileUtil.clearChildren(tempBin);
//			service.delete(Bootstrap.getProperty(Bootstrap.ID), "/src");
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	// 提交修改
//	protected void doSubmit() {
//		try {
//			IDesignCommonService service = RmiBeanFactory
//					.getBean(IDesignCommonService.class);
//			String clientId = Bootstrap.getProperty(Bootstrap.ID);
//			URL src = new URL(Bootstrap.getProperty(Bootstrap.SRC));
//			URL sitejar = new URL(Bootstrap.getClientSite(), "temp/" + clientId
//					+ ".temp.jar");
//			File localjar = new File(Bootstrap.getNativeTemp(), clientId
//					+ ".temp.jar");
//			if (FileUtil.isExixts(sitejar)) {
//				FileUtil.copy(sitejar.openStream(), new FileOutputStream(
//						localjar));
//			} else {
//				localjar.createNewFile();
//			}
//			FileUtil.addFileToZip(localjar, new File(Bootstrap.getNativeTemp(),
//					"bin"));
//			String protocol = sitejar.getProtocol();
//			if (protocol.equals("file")) {
//				// 替换站点临时文件
//				FileUtil.copy(new FileInputStream(localjar),
//						new FileOutputStream(new File(sitejar.getFile())));
//			}
//
//			// 提交Src
//			String pro = src.getProtocol();
//			if (pro.equals("file")) {
//				String localSrc = service.getLocation(clientId, "/src");
//				File sourceFolder = new File(localSrc);
//				File targetFolder = new File(src.getFile());
//				FileUtil.copy(sourceFolder, targetFolder);
//			}
//			MessageBox box = new MessageBox(shell);
//			box.setText("提交成功");
//			box.setMessage("源码已提交至:" + src.getPath() + "\r\n" + "编译文件已部署至:"
//					+ sitejar.getPath());
//			box.open();
//		} catch (RemoteException e) {
//			MessageBox box = new MessageBox(shell, SWT.ICON_ERROR);
//			box.setText("RMI异常");
//			box.setMessage("未找到对应的RMI服务，请检查设计工具是否启动\r\n " + e.getMessage());
//			box.open();
//		} catch (Exception e) {
//			e.printStackTrace();
//			MessageBox box = new MessageBox(shell, SWT.ICON_ERROR);
//			box.setText("异常");
//			box.setMessage("提交出错:" + e.getMessage());
//			box.open();
//		}
//	}
//
//	protected void doEdit() {
//		try {
//			IDesignCommonService service = RmiBeanFactory
//					.getBean(IDesignCommonService.class);
//			URL src = new URL(Bootstrap.getProperty(Bootstrap.SRC));
//			String widgetName = widget.getClass().getName();
//			URL[] classpaths = Bootstrap.getClassPathLibs();
//
//			URL sitejar = new URL(Bootstrap.getClientSite(), "temp/"
//					+ Bootstrap.getProperty(Bootstrap.ID) + ".temp.jar");
//
//			if (FileUtil.isExixts(sitejar)) {
//				classpaths = Arrays.copyOf(classpaths, classpaths.length + 1);
//				classpaths[classpaths.length - 1] = sitejar;
//			}
//
//			service.openWidgetEdit(Bootstrap.getNativeTemp(),
//					Bootstrap.getProperty(Bootstrap.ID), src, classpaths,
//					widgetName);
//		} catch (RemoteException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
	public void setWidget(IDesignOnLine widget) {
		this.widget = widget;
		tree.removeAll();
		TreeItem item = new TreeItem(tree, SWT.NONE);
		item.setText(new String[] { "Class", widget.getClass().getSimpleName() });
	}
}
