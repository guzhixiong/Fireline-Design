package com.zgw.fireline.base.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.zgw.fireline.base.widgets.TextPopup;

public class TestControlPopup extends TextPopup {
	private Table table;
	private Text text;

	public TestControlPopup(Text control, Listener listener) {
		super(control, listener);
	}

	public void doSelectUp() {
		int index = table.getSelectionIndex() - 1;
		if (index >= 0 && index < table.getItemCount())
			table.setSelection(index);
	}

	public void doSelectDown() {
		int index = table.getSelectionIndex() + 1;
		if (index >= 0 && index < table.getItemCount())
			table.setSelection(index);
	}

	@Override
	protected void doFilter(String trim) {

	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected Control createContent(Composite parent) {
		Composite content = new Composite(parent, SWT.BORDER);
		GridLayout gl_content = new GridLayout(1, false);
		gl_content.verticalSpacing = 0;
		gl_content.marginHeight = 0;
		gl_content.horizontalSpacing = 0;
		gl_content.marginWidth = 0;
		content.setLayout(gl_content);

		table = new Table(content, SWT.FULL_SELECTION);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableItem tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText("New TableItem");

		TableItem tableItem_1 = new TableItem(table, SWT.NONE);
		tableItem_1.setText("New TableItem");

		TableItem tableItem_2 = new TableItem(table, SWT.NONE);
		tableItem_2.setText("New TableItem");

		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("New Column");

		TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_1.setWidth(100);
		tblclmnNewColumn_1.setText("New Column");

		text = new Text(content, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label label = new Label(content, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1,
				1));

		Composite composite = new Composite(content, SWT.NONE);
		RowLayout rl_composite = new RowLayout(SWT.HORIZONTAL);
		composite.setLayout(rl_composite);
		GridData gd_composite = new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1);
		gd_composite.verticalIndent = 5;
		composite.setLayoutData(gd_composite);

		Button btnNewButton_1 = new Button(composite, SWT.NONE);
		btnNewButton_1.setText("New Button");

		Button btnNewButton = new Button(composite, SWT.NONE);
		btnNewButton.setText("New Button");
		return content;
	}

	public static void main(String[] args) {
		Shell shell = new Shell(SWT.APPLICATION_MODAL | SWT.SHELL_TRIM);
		Text text = new Text(shell, SWT.BORDER);
		TestControlPopup popup = new TestControlPopup(text, new Listener() {
			public void handleEvent(Event event) {

			}
		});
		text.setBounds(20, 50, 100, 20);
		shell.setSize(400, 400);
		Text text2 = new Text(shell, SWT.BORDER);

		text2.setBounds(30, 30, 100, 20);
		Listener doubleListener = new Listener() {
			public void handleEvent(Event event) {
				Text t = (Text) event.widget;
				StringBuffer sb = new StringBuffer(t.getText());
				sb.replace(event.start, event.end, event.text);
				String s = sb.toString();
				if (s.trim().equals(""))
					return;
				try {
					Double.parseDouble(s);
				} catch (NumberFormatException e) {
					event.doit = false;
				}
			}
		};
		text2.addListener(SWT.Verify, doubleListener);
		shell.open();
		while (!shell.isDisposed()) {
			if (!shell.getDisplay().readAndDispatch()) {
				shell.getDisplay().sleep();
			}
		}

	}

	@Override
	protected void okPressed() {
		// TODO Auto-generated method stub
		
	}
}
