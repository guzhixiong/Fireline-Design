package com.zgw.fireline.base.dataset;

import javax.sql.rowset.CachedRowSet;

public interface IRowsetProvide {
	public CachedRowSet queryForRowset(CachedRowSet set);
}
