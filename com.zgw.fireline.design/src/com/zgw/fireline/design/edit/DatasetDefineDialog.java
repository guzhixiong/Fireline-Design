package com.zgw.fireline.design.edit;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.internal.core.DesignerPlugin;
import org.eclipse.wb.internal.core.utils.execution.ExecutionUtils;
import org.eclipse.wb.internal.core.utils.reflect.ReflectionUtils;

import com.zgw.fireline.design.Model.IDataBaseProvideHelp;
import com.zgw.fireline.design.common.ExecutionUtils2;

public class DatasetDefineDialog extends Dialog {
	private Text cmdText;
	private Text numberText;
	private Text nameText;
	private Tree tree;
	private Class<?> provide;
	private ToolItem addItem;
	private ToolItem editItem;
	private ToolItem saveItem;
	private ToolItem cancelItem;
	private ToolItem delItem;
	private int state = 0; // 0浏览状态 1 修改 状态 2 新增
	private Text text;

	public Object define;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public DatasetDefineDialog(Shell parentShell, Class<?> provide) {
		super(parentShell);
		setShellStyle(SWT.MAX | SWT.RESIZE);
		this.provide = provide;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));

		ToolBar toolBar = new ToolBar(container, SWT.FLAT | SWT.RIGHT);

		addItem = new ToolItem(toolBar, SWT.NONE);
		addItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doAdd();
			}
		});
		addItem.setText("新增");

		editItem = new ToolItem(toolBar, SWT.NONE);
		editItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doEdit();
			}
		});
		editItem.setText("修改");

		saveItem = new ToolItem(toolBar, SWT.NONE);
		saveItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSave();
			}
		});
		saveItem.setText("保存");

		cancelItem = new ToolItem(toolBar, SWT.NONE);
		cancelItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doCancel();
			}
		});
		cancelItem.setText("取消");

		delItem = new ToolItem(toolBar, SWT.NONE);
		delItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doRemove();
			}
		});
		delItem.setText("删除");

		SashForm sashForm = new SashForm(container, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
				1));

		Composite composite = new Composite(sashForm, SWT.NONE);
		GridLayout gl_composite = new GridLayout(1, false);
		gl_composite.marginWidth = 0;
		gl_composite.marginHeight = 0;
		composite.setLayout(gl_composite);

		text = new Text(composite, SWT.BORDER);
		text.setMessage("输入编号名称进行查找");
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		tree = new Tree(composite, SWT.BORDER);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSelection();
			}
		});

		Composite composite_1 = new Composite(sashForm, SWT.BORDER);
		GridLayout gl_composite_1 = new GridLayout(2, false);
		composite_1.setLayout(gl_composite_1);

		Label lblNewLabel = new Label(composite_1, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel.setText("编号：");

		numberText = new Text(composite_1, SWT.BORDER);
		numberText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel_1 = new Label(composite_1, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel_1.setText("名称：");

		nameText = new Text(composite_1, SWT.BORDER);
		nameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblNewLabel_2 = new Label(composite_1, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false,
				false, 1, 1));
		lblNewLabel_2.setText("命令语句：");

		cmdText = new Text(composite_1, SWT.BORDER | SWT.MULTI);
		cmdText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		sashForm.setWeights(new int[] { 3, 7 });
		getShell().setText("系统数据集管理");
		initializeData();
		updateEnable();
		return container;
	}

	//
	protected void doSelection() {
		if (tree.getSelectionCount() > 0) {
			TreeItem item = tree.getSelection()[0];
			Object define = item.getData();
			String id = ReflectionUtils.getFieldString(define, "id");
			numberText.setText(id == null ? "" : id);
			String name = ReflectionUtils.getFieldString(define, "name");
			nameText.setText(name == null ? "" : name);
			String command = ReflectionUtils.getFieldString(define, "command");
			cmdText.setText(command == null ? "" : command);
		} else {
			numberText.setText("");
			nameText.setText("");
			cmdText.setText("");
		}
	}

	protected void doCancel() {
		if (state == 2) {// 如果是新增就删除选中项
			TreeItem item = tree.getSelection()[0];
			item.dispose();
		}
		if (tree.getSelectionCount() > 0)
			doSelection();
		else
			doSelection();
		state = 0;
		updateEnable();
	}

	protected void doEdit() {
		if (tree.getSelectionCount() > 0) {
			state = 1;
			updateEnable();
		} else {
			MessageDialog.openInformation(getShell(), "操作提示 ", "请选择一条记录");
		}
	}

	private void initializeData() {
		try {
			List list = IDataBaseProvideHelp.getAllDataset(provide);
			for (Object define : list) {
				TreeItem item = new TreeItem(tree, SWT.NONE);
				String name = ReflectionUtils.getFieldString(define, "name");
				item.setText(name == null ? "" : name);
				item.setData(define);
			}
			if (tree.getItems().length > 0) {
				tree.setSelection(tree.getItem(0));
				doSelection();
			}
		} catch (Exception e) {
			DesignerPlugin.log(e);
			MessageDialog.openError(getShell(), "错误",
					"数据集定义加载失败" + e.getMessage());
		}
	}

	protected void doRemove() {
		TreeItem item = tree.getSelection()[0];
		Object define = item.getData();
		try {
			IDataBaseProvideHelp.removeDatasetDefine(provide, define);
			item.dispose();
		} catch (Exception e) {
			DesignerPlugin.log(e);
			MessageDialog.openError(getShell(), "错误", "删除失败:" + e.getMessage());
		}

	}

	protected void doSave() {
		TreeItem item = tree.getSelection()[0];
		Object define = item.getData();
		ReflectionUtils.setField(define, "id", numberText.getText());
		ReflectionUtils.setField(define, "name", nameText.getText());
		ReflectionUtils.setField(define, "command", cmdText.getText());
		ReflectionUtils.setField(define, "typeId", "0");
		try {
			IDataBaseProvideHelp.saveDatasetDefine(provide, define);
			item.setText(nameText.getText());
			state = 0;
			updateEnable();
		} catch (Exception e) {
			DesignerPlugin.log(e);
			MessageDialog.openError(getShell(), "错误", "保存失败:" + e.getMessage());
		}
	}

	protected void doAdd() {
		state = 2;
		updateEnable();
		TreeItem item = new TreeItem(tree, SWT.NONE);
		try {
			Object define = IDataBaseProvideHelp.createDatasetDefine(provide);
			item.setData(define);
			tree.setSelection(item);
			doSelection();
			numberText.setFocus();
		} catch (Exception e) {
			throw ReflectionUtils.propagate(e);
		}

	}

	// state 状态 0浏览1 修改 2 新增
	private void updateEnable() {
		int count = tree.getSelectionCount();
		addItem.setEnabled(state == 0);
		editItem.setEnabled(count != 0 && state == 0);
		saveItem.setEnabled(state == 1 || state == 2);
		cancelItem.setEnabled(state == 1 || state == 2);
		delItem.setEnabled(count != 0 && state == 0);
		numberText.setEditable(state == 2);
		nameText.setEditable(state == 1 || state == 2);
		cmdText.setEditable(state == 1 || state == 2);
		if (getButton(IDialogConstants.OK_ID) != null)
			getButton(IDialogConstants.OK_ID).setEnabled(
					tree.getSelectionCount() > 0 && state == 0);
	}

	@Override
	protected void okPressed() {
		if (tree.getSelectionCount() > 0 && state == 0) {
			define = tree.getSelection()[0].getData();
			super.okPressed();
		}
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "确定", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "关闭", false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(625, 448);
	}
}
