package com.zgw.fireline.design.test;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import com.sun.rowset.CachedRowSetImpl;

public class TestDialog extends Dialog {
	private DataBindingContext m_bindingContext;
	protected Object result;
	protected Shell shell;

	private CachedRowSetImpl set;
	private BindBean bindBean_1 = new BindBean();

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public TestDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE
				| SWT.APPLICATION_MODAL);
		shell.setSize(439, 313);
		shell.setText(getText());
		bindBean_1 = new BindBean();
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));

		Menu menu = new Menu(shell);
		shell.setMenu(menu);

		MenuItem mntmNewItem = new MenuItem(menu, SWT.NONE);
		mntmNewItem.setText("New Item");

		MenuItem mntmNewItem_1 = new MenuItem(menu, SWT.NONE);
		mntmNewItem_1.setText("New Item");

		final StyledText styledText = new StyledText(shell, SWT.BORDER);
		styledText.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent e) {
				int offset = -1;
				try {
					offset = styledText
							.getOffsetAtLocation(new Point(e.x, +e.y));
				} catch (Exception e1) {
				}
				Cursor cursor = null;
				if (offset >= 0 && offset < styledText.getCharCount()) {
					StyleRange range = styledText.getStyleRangeAtOffset(offset);
				}
			}
		});

	}

	public static void main(String[] args) {
		TestDialog t = new TestDialog(new Shell(), SWT.NONE);
		t.open();
	}

}
