package com.zgw.fireline.design.Model;

import java.util.Calendar;
import java.util.List;

import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.core.model.JavaInfo;
import org.eclipse.wb.internal.core.model.JavaInfoUtils;
import org.eclipse.wb.internal.core.model.creation.CreationSupport;
import org.eclipse.wb.internal.core.model.description.ComponentDescription;
import org.eclipse.wb.internal.core.model.description.GenericPropertyDescription;
import org.eclipse.wb.internal.core.model.property.GenericPropertyImpl;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.TextDialogPropertyEditor;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.ast.AstNodeUtils;
import org.eclipse.wb.internal.swt.model.widgets.CompositeInfo;

import com.zgw.fireline.design.edit.ShowTextPropertyDialog;

/**
 * 带数据源的下拉框模型
 * */
public class DataComboInfo extends CompositeInfo {

	public DataComboInfo(AstEditor editor, ComponentDescription description,
			CreationSupport creationSupport) throws Exception {
		super(editor, description, creationSupport);
		initDescriptor();
	}

	// 配置 Descriptor
	private void initDescriptor() {
		for (GenericPropertyDescription p : getDescription().getProperties()) {
			if (p.getId().equals("setShowText(java.lang.String)")) {
				p.setEditor(ShowTexPropertytEditor.Instance);
			}
		}
	}

	@Override
	protected List<Property> getPropertyList() throws Exception {
		// TODO Auto-generated method stub
		return super.getPropertyList();
	}

	public DatasetInfo getDataset() {
		TypeDeclaration typeDeclaration = JavaInfoUtils
				.getTypeDeclaration(getRootJava());
		ClassInstanceCreation c = (ClassInstanceCreation) getCreationSupport()
				.getNode();
		SimpleName s = (SimpleName) c.arguments().get(2);
		if (s != null) {
			return (DatasetInfo) getRootJava().getChildRepresentedBy(s);
			// VariableDeclarationFragment node = AstNodeUtils
			// .getFieldFragmentByName(typeDeclaration,
			// s.getFullyQualifiedName());
			// JavaInfo datasetinfo = DatasetCodeUtil.getJavaInfoByASTNode(
			// getRootJava(), node);
			// return (DatasetInfo) datasetinfo;
		}
		return null;
	}

	private static class ShowTexPropertytEditor extends
			TextDialogPropertyEditor {
		static ShowTexPropertytEditor Instance = new ShowTexPropertytEditor();

		@Override
		protected void openDialog(Property property) throws Exception {
			GenericPropertyImpl gp = (GenericPropertyImpl) property;
			DataComboInfo info = (DataComboInfo) gp.getJavaInfo();
			ShowTextPropertyDialog dialog = new ShowTextPropertyDialog(Display
					.getDefault().getActiveShell(), info.getDataset());
			dialog.showText = (String) property.getValue();
			if (dialog.open() == Window.OK) {
				property.setValue(dialog.showText);
			}
		}

		@Override
		protected String getText(Property property) throws Exception {
			return (String) property.getValue();
		}
	}
}
